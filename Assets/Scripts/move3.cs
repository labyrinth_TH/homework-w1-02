﻿using UnityEngine;
using System.Collections;

public class move3 : MonoBehaviour {

	public float TotalTime;

	void Awake() {
		Debug.Log ("Hello world - Awake");
	}
	
	void Start() {
		Debug.Log ("Hello world - Start");
	}
	
	void Update() {
		TotalTime += Time.deltaTime; 
		
		Vector3 position = transform.position;
		Debug.Log (position.x);
		Debug.Log (position.y);
		Debug.Log (position.z);
		if (TotalTime < 2) {
						position.y += 0.04f;
				} else if (TotalTime < 4) {
						position.y += 0.04f;
						position.x += 0.04f;
				} else if (TotalTime < 6) {
						position.y -= 0.04f;
						position.x += 0.04f;
				} else if (TotalTime < 8) {
						position.y -= 0.04f;
				} else if (TotalTime < 10) {
						position.x -= 0.04f;
						position.y -= 0.04f;
				} else if (TotalTime < 12) {
						position.x -= 0.04f;
						position.y += 0.04f;
				}
		if (TotalTime > 12) {
			TotalTime = 12;
		}
		transform.position = position;
	}
}