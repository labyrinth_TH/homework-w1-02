﻿using UnityEngine;
using System.Collections;

public class button1 : MonoBehaviour {

	public float speed;

	// Update is called once per frame
	void Update () {
		float speedFactor = 5;
		if (Input.GetKey (KeyCode.LeftShift) ||
						Input.GetKey (KeyCode.RightShift)) {
						speedFactor = 10;
				}

		if (Input.GetKey (KeyCode.W)) {
						Vector3 p = transform.position;
						p.z += speedFactor * speed * Time.deltaTime;
						transform.position = p;
				}
		if (Input.GetKey (KeyCode.S)) {
						Vector3 p = transform.position;
						p.z -= speedFactor * speed * Time.deltaTime;
						transform.position = p;
				}

		if (Input.GetKey (KeyCode.A)) {
						Vector3 p = transform.position;
						p.x -= speedFactor * speed * Time.deltaTime;
						transform.position = p;
				}
		if (Input.GetKey (KeyCode.D)){
			Vector3 p = transform.position;
			p.x += speedFactor * speed * Time.deltaTime;
			transform.position = p;
		}
	}
}
