﻿using UnityEngine;
using System.Collections;

public class move1 : MonoBehaviour {

	public float TotalTime;

	void Awake() {
		Debug.Log ("Hello world - Awake");
	}

	void Start() {
		Debug.Log ("Hello world - Start");
	}

	public bool ball = false;

	void Update() 
	{
		if (Input.GetKey(KeyCode.Space)) 
		{
			ball = true;
		}
		if (ball) 
		{
			TotalTime += Time.deltaTime; 

			Vector3 position = transform.position;
			Debug.Log (position.x);
			Debug.Log (position.y);
			Debug.Log (position.z);
			if (TotalTime < 2) {
					position.y += 0.06f;
					position.x += 0.02f;
			} else if (TotalTime < 4) {
					position.y -= 0.06f;
					position.x += 0.02f;
			} else if (TotalTime < 6) {
					position.y += 0.04f;
					position.x -= 0.08f;
			} else if (TotalTime < 8) {
					position.x += 0.10f;
			} else if (TotalTime < 10) {
					position.y -= 0.04f;
					position.x -= 0.08f;
			}
			if (TotalTime > 10) {
					ball = false;
			}
			transform.position = position;
		} 
		else 
		{
			TotalTime = 0.0f;
		}
	}
}
