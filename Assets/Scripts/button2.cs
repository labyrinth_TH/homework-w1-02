﻿using UnityEngine;
using System.Collections;

public class button2 : MonoBehaviour {

	public float Speed;
	
	// Update is called once per frame
	void Update () {
		float h = Input.GetAxis ("Horizontal");
		float v = Input.GetAxis ("Vertical");

		Vector3 p = transform.position;
		p.x += h * Speed * Time.deltaTime;
		p.z += v * Speed * Time.deltaTime;
		transform.position = p;
	}
}
