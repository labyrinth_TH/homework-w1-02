﻿using UnityEngine;
using System.Collections;

public class move2 : MonoBehaviour {

	public float TotalTime;
	
	void Awake() {
		Debug.Log ("Hello world - Awake");
	}
	
	void Start() {
		Debug.Log ("Hello world - Start");
	}
	
	void Update() {
				TotalTime += Time.deltaTime; 
			
			Vector3 position = transform.position;
			Debug.Log (position.x);
			Debug.Log (position.y);
			Debug.Log (position.z);
			if (TotalTime < 2) {
				position.y += 0.08f;
			} else if (TotalTime < 4) {
				position.y -= 0.10f;
				position.x += 0.05f;
			} else if (TotalTime < 6) {
				position.y += 0.10f;
			} else if (TotalTime < 8) {
				position.x -= 0.08f;
			    position.y -= 0.08f;
			}
			if (TotalTime > 8) {
				TotalTime = 8;
			}
			transform.position = position;
	}
}
